def validate_fields(d):
    fls = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    return all([i in d for i in fls])

def validate_range(d, h, l, k):
    return k in d and int(d[k]) in range(l, h + 1)

def validate_bry(d):
    return validate_range(d, 2002, 1920, "byr")

def validate_iyr(d):
    return validate_range(d, 2020, 2010, "iyr")

def validate_eyr(d):
    return validate_range(d, 2030, 2020, "eyr")

def validate_hgt(d):
    return 'hgt' in d and ('cm' in d['hgt'] and validate_range({"hgt": int(d["hgt"][0:-2])}, 193, 150, "hgt") or
                           'in' in d['hgt'] and validate_range({"hgt": int(d["hgt"][0:-2])}, 76, 59, "hgt"))

def validate_hcl(d):
    return 'hcl' in d and len(d['hcl']) == 7 and d['hcl'][1:].isalnum()

def validate_pid(d):
    return 'pid' in d and len(d['pid']) == 9 and d['pid'].isdigit()

def validate_ecl(d):
    return 'ecl' in d and d['ecl'] in {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}


f = open('i4.fixed').readline().split("$")
rd = [{k: v for (k, v) in [(j.split(":")[0], j.split(":")[1]) for j in d.split(' ') if j != '']} for d in f]


all_validations = [validate_bry, validate_ecl, validate_eyr, validate_hcl, validate_hgt,
                   validate_iyr, validate_pid, validate_fields]

x = 0
for d in rd:
    valid = []
    for v in all_validations:
        valid.append(v(d))
    x += all(valid)
print(x)