def add_of_size(n, target, f):
    for i in range(0, len(f)):
        if sum(f[i:i+n]) == target:
            print(min(f[i:i+n]) + max(f[i:i+n]))
            return True

def silver(f):
    for i in range(0, len(f)):
        s = f[i:i+25]
        prodset = set([s[i] + s[j] for i in range(25) for j in range(25) if i != j])
        if f[i+25] not in prodset:
            return f[i+25], f


def gold(target, f):
    for i in range(2, len(f)):
        if add_of_size(i, target, f):
            return

gold(*silver([int(i) for i in open("d9.input")]))