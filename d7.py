import re

def parse(l):
    l = l.replace("contain", ",").replace("bags", "").replace("bag", "")
    l = [i.rstrip() for i in re.sub(' +', ' ', l).split(" , ")]
    l = [l[0]] + parsedep(l[1:])
    return l

def parsedep(l):
    return l if l == ["no other"] else [(int(i[0]), i[2:]) for i in l]

def silver(bag_list):

    def helper(key):
        if "shiny gold" in bag_dict[key]:
            return True
        elif "no other" in bag_dict[key]:
            return False
        else:
            return any(map(helper, bag_dict[key]))

    bag_dict = {}
    for bag in bag_list:
        bag_dict[bag[0]] = [i[1].lstrip() for i in bag[1:] if i != "no other"]
    print(sum([*map(helper, bag_dict)]))


def gold(bag_list):

    def helper(key):
        return 1 if "no other" in bag_dict[key] else 1 + \
        sum([bag[0] * helper(bag[1]) for bag in bag_dict[key]])

    bag_dict = {}
    for bag in bag_list:
        bag_dict[bag[0]] = [i for i in bag[1:]]

    print(helper("shiny gold") - 1)


data = [i.split("\n")[0].rstrip(".") for i in open("d7.input")]
parsed = [parse(i) for i in data]

silver(parsed)
gold(parsed)
