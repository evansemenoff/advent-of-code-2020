main = do
    filecontent <- readFile "d3.input"
    let slopes = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)] 
    let field = lines filecontent
    let solver = uncurry(solve field 0 0)
    print $ foldl (*) 1 (map solver slopes)

solve f x y a b 
    | x >= length f = 0 
    | spot == '#'   = 1 + solve f x2 y2 a b 
    | spot == '.'   = 0 + solve f x2 y2 a b 
    where spot = f!!x!!y
          x2 = x + a 
          y2 = (y + b) `mod` (length $ f!!0)
