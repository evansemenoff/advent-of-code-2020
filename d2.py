def parse(l):
    d = l.split()
    obj = lambda: None
    obj.interval = range(int(d[0].split('-')[0]),
                         int(d[0].split('-')[1]) + 1)
    obj.letter = d[1].rstrip(':')
    obj.string = d[2]
    obj.count = obj.string.count(obj.letter)
    return obj

def part1(s):
    return s.count in s.interval

def part2(s):
    def test(index):
        return True if len(s.string) >= index and\
                       s.string[index - 1] == s.letter else False
    return test(s.interval.stop - 1) ^ test(s.interval.start)

def main():
    parsed = [*map(parse, open("i2"))]
    solve = lambda x: sum(map(x, parsed))
    print(solve(part1), solve(part2))

main()