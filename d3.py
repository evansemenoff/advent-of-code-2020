from math import prod


def g(a, b, x, y):
    if x >= len(field):
        return 0
    else:
        return (field[x][y] == '#') + g(a, b, x + a, (y + b) % len(field[0]))


inputs = [[1, 1], [1, 3], [1, 5], [1, 7], [2, 1]]
field = [line.rstrip() for line in open("i3")]
print(prod([g(*i, 0, 0) for i in inputs]))

