from collections import defaultdict


def silver(s):
    ones = 0
    threes = 0
    for i in range(len(s) - 1):
        if s[i + 1] - s[i] == 1:
            ones += 1
        elif s[i + 1] - s[i] == 3:
            threes += 1
    return ones * threes


def gold(l):
    d = defaultdict(int)
    d[0] = 1
    for i in l:
        d[i] += d[i - 1] + d[i - 2] + d[i - 3]
    return d[l[-1]]


def main():
    f = [int(i) for i in open("d10.in")]
    s = [0] + sorted(f)
    s = s + [s[-1] + 3]

    print(silver(s))
    print(gold(s))


main()