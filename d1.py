from itertools import product
from functools import reduce
print((lambda f: reduce(lambda a, b: a*b, [*filter(lambda a: sum(a) == 2020, f)][0]))
      (product(map(lambda z: int(z), open("i1")), repeat=2)))