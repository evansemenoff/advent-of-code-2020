import Data.List.Split
import Data.List
main = do
    filed <- readFile "d6.in"
    let seperated = map (splitOn "\n") (splitOn "\n\n" filed)
    print $ solve intersect seperated
    print $ solve union seperated

solve f = sum . map ( length . foldr1 f ) 
