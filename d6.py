from functools import reduce
from operator import and_, or_

def solve(f):
    return lambda l: len(reduce(f, map(set, l)))

def main():
    f = open("d6.input")
    o = f.read().split("\n\n")
    data = [i.split("\n") for i in o]
    print(sum([*map(solve(or_), data)]),
          sum([*map(solve(and_), data)]))

main()