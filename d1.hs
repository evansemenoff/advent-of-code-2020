main = do
    filecontent <- readFile "d1"
    let ints = map (read::String->Int) (lines filecontent)
    print . head $ (solve ints 2)
    print . head $ (solve ints 3)

solve is n
    | n == 2 = [ i*j | i <- is, j <- is, i+j == 2020 ]
    | n == 3 = [ i*j*k | i <- is, j <- is, k <- is, i+j+k == 2020 ]