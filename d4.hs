import Data.List.Split
import Data.List
import Data.Maybe
import Data.Char (isDigit)

main = do
    filecontent <- readFile "d4.fixed"
    let seperated = head $ map (splitOn "$") (lines filecontent)
    let split   = map (splitOn " ") seperated
    let paired = map parseone split
    let valids = map validate paired
    print $ length $ filter (/=Nothing) valids

parseone l = Just (Just [ i | i <- map (splitOn ":") l, i /= [""]] )

validate d = d >>= validateFields
               >>= validateByr
               >>= validateEyr
               >>= validateIyr
               >>= validateHgt
               >>= validateHcl
               >>= validatePid

validateFields d
    | valid     = Just d
    | otherwise = Nothing
    where l     = [ i !! 0 | i <- fromJust d, i !! 0 /= "cid" ]
          req   = ["byr" ,"ecl" ,"eyr" ,"hcl" ,"hgt" ,"iyr" ,"pid"] 
          valid = sort l == req


validateEyr d = validRange "eyr" 2020 2030 d

validateByr d = validRange "byr" 1920 2002 d

validateIyr d = validRange "iyr" 2010 2020 d

validatePid d
    | valid     = Just d
    | otherwise = Nothing
    where l     = [ i !! 1 | i <- fromJust d, i !! 0 == "pid" ]
          valid 
            | l /= []   = (length (l !! 0) == 9) && (all isDigit (l !! 0))
            | otherwise = False


validateHgt d
   | valid     = Just d
   | otherwise = Nothing
   where l     = [ i !! 1 | i <- fromJust d, i !! 0 == "hgt" ]
         valid
            | l == []                  = False
            | (length (l !! 0)) <= 2   = False
            | last l == "n"            = n >= 59  && 76  >= n
            | otherwise                = n >= 150 && 193 >= n
            where n = (read $ reverse $ (tail . tail) $ reverse (l !! 0))

validateHcl d
    | valid     = Just d
    | otherwise = Nothing
    where l     = [ i !! 1 | i <- fromJust d, i !! 0 == "hcl"]
          valid
            | length l > 0 = ((take 1 (l !! 0)) == "#") && (length (l !! 0) == 7)
            | otherwise    = False

validRange t l h d 
    | h >= q && q >= l = Just d
    | otherwise        = Nothing
    where z            = [ i !! 1 | i <- fromJust d, (i !! 0) == t ]
          q 
            | length z > 0 = (read (z !! 0))

