from copy import deepcopy


def run_sim():
    ptr, acc = 0, 0
    while True:
        if ptr == len(inp):
            return acc
        elif inp[ptr][2]:
            return False

        inp[ptr][2] = True
        if inp[ptr][0] == "jmp":
            ptr += inp[ptr][1]
        elif inp[ptr][0] == "acc":
            acc += inp[ptr][1]
            ptr += 1
        elif inp[ptr][0] == "nop":
            ptr += 1


def replace_and_run(ptr):
    global inp
    inp = deepcopy(original)
    if inp[ptr][0] == "jmp":
        inp[ptr][0] = "nop"
    elif inp[ptr][0] == "nop":
        inp[ptr][0] = "jmp"
    out = run_sim()
    return out if out else False


original = [[i[0], int(i[1]), False] for i in [i.split() for i in open("d8.input")]]
inp = deepcopy(original)
nop_jmp_ptrs = [i for i in range(len(inp)) if inp[i][0] == "jmp" or inp[i][0] == "nop"]
print([i for i in map(replace_and_run, nop_jmp_ptrs) if i])