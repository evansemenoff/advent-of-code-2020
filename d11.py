def iterate(f, g):
    dupe = [[i for i in y] for y in f]
    for i in range(1, len(f) - 1):
        for j in range(1, len(f[0]) - 1):
            occupied = g(f, i, j)
            if f[i][j] == "L" and occupied == 0:
                dupe[i][j] = "#"
            elif f[i][j] == "#" and occupied >= (5 if g.__name__ == "gold" else 4):
                dupe[i][j] = "L"
    return dupe


def silver(f, i, j):
    nbhd = [f[i + x][j + y] for x in range(-1, 2)
            for y in range(-1, 2) if not (y == 0 and x == 0)]
    return len([*filter(lambda x: x == "#", nbhd)])


def gold(f, i, j):

    def cast_ray(i, j, di, dj):
        i += di
        j += dj
        current = f[i][j]
        while current == ".":
            current = f[i][j]
            i += di
            j += dj
        return current

    nbhd = [cast_ray(i, j, x, y) for x in range(-1, 2)
            for y in range(-1, 2) if not (y == 0 and x == 0)]
    return len([*filter(lambda x: x == "#", nbhd)])


def is_equal(f1, f2):
    for i in range(len(f1)):
        for j in range(len(f1[0])):
            if f1[i][j] != f2[i][j]:
                return False
    return True


def main():
    field = [list("b" + i.rstrip() + "b") for i in open("d11.in")]
    field.insert(0, [*("b" * len(field[0]))])
    field.append([*"b" * len(field[0])])

    old_field = [[[]]]
    while not is_equal(old_field, field):
        old_field = field
        field = iterate(field, silver)

    total = 0
    for i in field:
        total += len([*filter(lambda x: x == "#", i)])
    print(total)

main()
