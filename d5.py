def splitNsub(l):
    s = l.rstrip()
    x1 = int(s[0:7].replace('F', '0').replace('B', '1'), 2)
    x2 = int(s[7:].replace('L', '0').replace('R', '1'), 2)
    return x1*8 + x2

data = [*map(splitNsub, open("5.input"))]
print(set(sorted(data)[0:-1]) ^ set(range(min(data), max(data))))