import Data.List.Split
main = do
    filecontent <- readFile "d2.txt"
    let seperated = map (splitOn " ") (lines filecontent)
    let parsed = map parse seperated
    print(count True (map solve1 parsed))
    print(count True (map solve2 parsed))

count e l = length $ filter (==e) l

parse x = ( f $ splitOn "-" $ head x, head $ x!!1 , last x ) 
    where f x = map read x

solve1 (r, l, s) = count l s `elem` [r!!0..r!!1]

solve2 (r, l, s) = (test s (head r) l) /= (test s (last r) l)
    where test s i l 
            | length s >= i = s!!(i-1) == l
            | otherwise     = False
